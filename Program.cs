﻿/*
 * Copyright 2022 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

// Play FizzBuzz up to 100 places
const float defaultLimit = 100;
Play(defaultLimit);

void Play(float limit)
{
    // Define numbers whose multiples should be printed as a string instead of a number
    const float fizzValue = 3;
    const string fizzName = "Fizz";
    const float buzzValue = 5;
    const string buzzName = "Buzz";

    // Play the game until the limit is reached
    for (float i = 1; i <= limit; i++)
    {
        string output = "";

        // If there is no remainder after dividing the dividend and divisor, append input to output
        output = StringOrNumber(i, fizzValue, output, fizzName);
        output = StringOrNumber(i, buzzValue, output, buzzName);

        // If both Method calls resulted in a remainder, print the number
        if (String.IsNullOrEmpty(output))
        {
            output = i.ToString();
        }

        // Display the result
        Console.WriteLine(output);
    }
}

string StringOrNumber(float dividend, float divisor, string output, string input)
{
    if ((dividend % divisor) == 0)
    {
        output += input;
        return output;
    }
    else
    {
        // Return the string unmodified
        return output;
    }
}