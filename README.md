# FizzBuzz

Based on the Tom Scott video - [FizzBuzz: One Simple Interview Question](https://youtu.be/QPZ0pIK_wsc).

![A screenshot of FizzBuzz](screenshot.png)

## Technologies

- .NET 6
- C# 10

A cross-platform binary is available in `bin/Release/net6.0/publish`. If you have `dotnet` installed, run `fizzbuzz.dll`.